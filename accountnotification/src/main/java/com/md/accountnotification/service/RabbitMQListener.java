package com.md.accountnotification.service;

import com.md.accountnotification.model.Notification;
import com.md.accountnotification.model.NotificationMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;

@Service
@Slf4j
public class RabbitMQListener {

    @Autowired
    private NotificationService notificationService;

    @RabbitListener(queues = "${notification.queue}", containerFactory = "jsaFactory")
    public void consume(NotificationMessage notificationMessage){
        log.info("Notification Message:" + notificationMessage.toString());
        Notification notification = new Notification();
        notification.setMessage(notificationMessage.getMessage());
        notification.setStatus(notificationMessage.getStatusCode());
        notification.setCreatedTime(notificationMessage.getCreatedTime());
        log.info("Notification:" + notification.toString());
        notificationService.createNotification(notification);
    }

}
