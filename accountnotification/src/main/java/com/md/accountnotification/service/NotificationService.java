package com.md.accountnotification.service;

import com.md.accountnotification.model.Notification;
import org.springframework.stereotype.Service;

import java.util.List;

public interface NotificationService {

    List<Notification> getAllNotifications();

    Notification createNotification(Notification notification);

}
