package com.md.accountnotification;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccountnotificationApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccountnotificationApplication.class, args);
	}

}
