package com.md.accountnotification.controller;

import com.md.accountnotification.model.Notification;
import com.md.accountnotification.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("account-notification")
public class NotificationController {

    @Autowired
    private NotificationService notificationService;

    @GetMapping("/all")
    public ResponseEntity<List<Notification>> getAllNotifications(){
        List<Notification> notifications = notificationService.getAllNotifications();
        if (notifications != null){
            return ResponseEntity.ok(notifications);
        } else {
            return ResponseEntity.ok(Collections.emptyList());
        }
    }

    @PostMapping(value = "/notification", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Notification> createOne(@RequestBody Notification notification){
        return ResponseEntity.ok(notificationService.createNotification(notification));
    }

}
