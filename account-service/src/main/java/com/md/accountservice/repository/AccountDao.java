package com.md.accountservice.repository;

import com.md.accountservice.model.Account;

import java.math.BigDecimal;
import java.util.List;

public interface AccountDao {

    List<Account> getAccountsByCustomerNum(BigDecimal customerNum);

    void createAccount(Account account);

    boolean moneyTransfer(Account fromAccount, Account toAccount, BigDecimal money);

    Account readAccount(BigDecimal unitNum, BigDecimal accountNum);

    void deleteAccount(BigDecimal unitNum, BigDecimal accountNum);

}
