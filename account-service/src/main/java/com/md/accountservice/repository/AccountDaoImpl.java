package com.md.accountservice.repository;

import com.md.accountservice.model.Account;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
@Slf4j
public class AccountDaoImpl extends JdbcDaoSupport implements AccountDao {

    @Autowired
    DataSource dataSource;

    @PostConstruct
    private void init(){
        setDataSource(dataSource);
    }

    @Override
    public List<Account> getAccountsByCustomerNum(BigDecimal customerNum) {

        log.info("GetAccountsByCustomerNum");
        log.info("CustomerNum:" + customerNum.toString());

        String sql = "select * from account where customerNum = :customerNum";
        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("customerNum", customerNum);
        List<Map<String,Object>> rows = namedParameterJdbcTemplate.queryForList(sql, paramMap);
        List<Account> accounts = rows.stream().map(row -> {
            Account account = new Account();
            account.setId((String) row.get("Id"));
            account.setCustomerNum((BigDecimal) row.get("customerNum"));
            account.setUnitNum((BigDecimal) row.get("unitNum"));
            account.setAccountNum((BigDecimal) row.get("accountNum"));
            account.setFirstName((String) row.get("firstName"));
            account.setLastName((String) row.get("lastName"));
            account.setBalanceAmount((BigDecimal) row.get("balanceAmount"));
            return account;
        }).collect(Collectors.toList());

        return accounts;
    }

    @Override
    public void createAccount(Account account) {

        log.info("Create Account");
        log.info("Account:" + account.toString());

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
        String sql = "insert into account (id, customerNum, unitNum, accountNum, firstName, lastName, balanceAmount) " +
                "values (:id, :customerNum, :unitNum, :accountNum, :firstName, :lastName, :balanceAmount)";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("id", account.getId());
        paramMap.put("customerNum", account.getCustomerNum());
        paramMap.put("unitNum", account.getUnitNum());
        paramMap.put("accountNum", account.getAccountNum());
        paramMap.put("firstName", account.getFirstName());
        paramMap.put("lastName", account.getLastName());
        paramMap.put("balanceAmount", account.getBalanceAmount());

        namedParameterJdbcTemplate.update(sql, paramMap);
    }

    @Override
    @Transactional
    public boolean moneyTransfer(Account fromAccount, Account toAccount, BigDecimal money) {

        log.info("Money Transfer");
        log.info("From Account: " + fromAccount.toString());
        log.info("To Account  : " + toAccount.toString());
        log.info("Transaction : " + money.toString());

        if (fromAccount.getBalanceAmount().compareTo(money) < 0)
            return false;

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
        String updateAccountSql = "update account set balanceAmount = :balanceAmount where unitNum = :unitNum and accountNum = :accountNum";
        Map<String, Object> paramMapForSender = new HashMap<>();
        paramMapForSender.put("unitNum", fromAccount.getUnitNum());
        paramMapForSender.put("accountNum", fromAccount.getAccountNum());
        paramMapForSender.put("balanceAmount", fromAccount.getBalanceAmount().subtract(money));
        namedParameterJdbcTemplate.update(updateAccountSql, paramMapForSender);

        Map<String, Object> paramMapForReveiver = new HashMap<>();
        paramMapForReveiver.put("unitNum", toAccount.getUnitNum());
        paramMapForReveiver.put("accountNum", toAccount.getAccountNum());
        paramMapForReveiver.put("balanceAmount", toAccount.getBalanceAmount().add(money));
        namedParameterJdbcTemplate.update(updateAccountSql, paramMapForReveiver);
        return true;

    }

    @Override
    public Account readAccount(BigDecimal unitNum, BigDecimal accountNum) {

        log.info("Read Account");
        log.info("Unit-Account" + unitNum.toString() + "-" + accountNum.toString());

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
        String sql = "select * from account where unitNum = :unitNum and accountNum = :accountNum";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("unitNum", unitNum);
        paramMap.put("accountNum", accountNum);

        log.info("ParamMap:" + paramMap.toString());

        Account readAccount = namedParameterJdbcTemplate.queryForObject(sql, paramMap, new RowMapper<Account>() {
            @Override
            public Account mapRow(ResultSet resultSet, int i) throws SQLException {
                Account account = new Account();
                account.setId(resultSet.getString("id"));
                account.setCustomerNum(resultSet.getBigDecimal("customerNum"));
                account.setUnitNum(resultSet.getBigDecimal("unitNum"));
                account.setAccountNum(resultSet.getBigDecimal("accountNum"));
                account.setFirstName(resultSet.getString("firstName"));
                account.setLastName(resultSet.getString("lastName"));
                account.setBalanceAmount(resultSet.getBigDecimal("balanceAmount"));
                return account;
            }
        });

        return readAccount;
    }

    @Override
    public void deleteAccount(BigDecimal unitNum, BigDecimal accountNum) {

        log.info("Delete Account");
        log.info("Unit-Account" + unitNum.toString() + "-" + accountNum.toString());

        NamedParameterJdbcTemplate namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(getJdbcTemplate());
        String sql = "delete from account where unitNum = :unitNum and accountNum = :accountNum";
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("unitNum", unitNum);
        paramMap.put("accountNum", accountNum);
        namedParameterJdbcTemplate.update(sql, paramMap);
    }


}
