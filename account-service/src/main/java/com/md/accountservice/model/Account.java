package com.md.accountservice.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    private String id;
    private BigDecimal customerNum;
    private BigDecimal unitNum;
    private BigDecimal accountNum;
    private String firstName;
    private String lastName;
    private BigDecimal balanceAmount;


}
