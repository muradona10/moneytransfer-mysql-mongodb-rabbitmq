package com.md.accountservice.controller;

import com.md.accountservice.model.Account;
import com.md.accountservice.model.TransferBean;
import com.md.accountservice.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @GetMapping("/all/{customerNum}")
    public ResponseEntity<List<Account>> getAccountsByCustomerNum(@PathVariable("customerNum") BigDecimal customerNum) {
        List<Account> accounts = accountService.getAccountsByCustomerNum(customerNum);
        if (accounts != null) {
            return ResponseEntity.ok(accounts);
        } else {
            return ResponseEntity.ok(Collections.emptyList());
        }
    }

    @PostMapping("/create")
    public ResponseEntity<Account> createAccount(@RequestBody Account account){
        accountService.createAccount(account);
        return ResponseEntity.ok(account);
    }

    @GetMapping("/account")
    public ResponseEntity<Account> readAccount(@RequestParam(name = "unitNum", required = true) BigDecimal unitNum,
                                               @RequestParam(name = "accountNum", required = true) BigDecimal accountNum){
        Account account = accountService.readAccount(unitNum, accountNum);
        if (account != null)
            return ResponseEntity.ok(account);
        else
            return ResponseEntity.ok(null);
    }

    @PutMapping("/account")
    public void deleteAccount(@RequestParam(name = "unitNum", required = true) BigDecimal unitNum,
                                               @RequestParam(name = "accountNum", required = true) BigDecimal accountNum){
        accountService.deleteAccount(unitNum, accountNum);
    }

    @PostMapping("/transfer")
    public Boolean moneyTransfer(@RequestBody TransferBean transferBean){

        Account fromAccount = transferBean.getFromAccount();
        Account toAccount = transferBean.getToAccount();
        BigDecimal amount = transferBean.getTransactionAmount();

        if(accountService.readAccount(fromAccount.getUnitNum(), fromAccount.getAccountNum()) == null)
            return Boolean.FALSE;

        if(accountService.readAccount(toAccount.getUnitNum(), toAccount.getAccountNum()) == null)
            return Boolean.FALSE;

        if(amount == null)
            return Boolean.FALSE;

        return accountService.moneyTransfer(fromAccount, toAccount, amount);
    }


}
