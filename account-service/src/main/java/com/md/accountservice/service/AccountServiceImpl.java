package com.md.accountservice.service;

import com.md.accountservice.model.Account;
import com.md.accountservice.repository.AccountDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Override
    public List<Account> getAccountsByCustomerNum(BigDecimal customerNum) {
        return accountDao.getAccountsByCustomerNum(customerNum);
    }

    @Override
    public void createAccount(Account account) {
        accountDao.createAccount(account);
    }

    @Override
    public boolean moneyTransfer(Account fromAccount, Account toAccount, BigDecimal money) {
        return accountDao.moneyTransfer(fromAccount, toAccount, money);
    }

    @Override
    public Account readAccount(BigDecimal unitNum, BigDecimal accountNum) {
        return accountDao.readAccount(unitNum, accountNum);
    }

    @Override
    public void deleteAccount(BigDecimal unitNum, BigDecimal accountNum) {
        accountDao.deleteAccount(unitNum, accountNum);
    }


    @PostConstruct
    public void initData(){
        Account a1 = new Account();
        a1.setId(UUID.randomUUID().toString());
        a1.setCustomerNum(BigDecimal.valueOf(10));
        a1.setUnitNum(BigDecimal.valueOf(285));
        a1.setAccountNum(BigDecimal.valueOf(6000001));
        a1.setFirstName("Murat");
        a1.setLastName("Demir");
        a1.setBalanceAmount(BigDecimal.valueOf(50_000_000));
        accountDao.createAccount(a1);

        Account a2 = new Account();
        a2.setId(UUID.randomUUID().toString());
        a2.setCustomerNum(BigDecimal.valueOf(10));
        a2.setUnitNum(BigDecimal.valueOf(285));
        a2.setAccountNum(BigDecimal.valueOf(6000002));
        a2.setFirstName("Murat");
        a2.setLastName("Demir");
        a2.setBalanceAmount(BigDecimal.valueOf(10_000_000));
        accountDao.createAccount(a2);

        Account a3 = new Account();
        a3.setId(UUID.randomUUID().toString());
        a3.setCustomerNum(BigDecimal.valueOf(10));
        a3.setUnitNum(BigDecimal.valueOf(285));
        a3.setAccountNum(BigDecimal.valueOf(6000003));
        a3.setFirstName("Murat");
        a3.setLastName("Demir");
        a3.setBalanceAmount(BigDecimal.valueOf(5_000_000));
        accountDao.createAccount(a3);

        Account a4 = new Account();
        a4.setId(UUID.randomUUID().toString());
        a4.setCustomerNum(BigDecimal.valueOf(11));
        a4.setUnitNum(BigDecimal.valueOf(285));
        a4.setAccountNum(BigDecimal.valueOf(7000001));
        a4.setFirstName("Derya");
        a4.setLastName("Demir");
        a4.setBalanceAmount(BigDecimal.valueOf(50_000_000));
        accountDao.createAccount(a4);
    }



}
