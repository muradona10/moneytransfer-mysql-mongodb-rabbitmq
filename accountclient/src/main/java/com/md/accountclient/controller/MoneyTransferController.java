package com.md.accountclient.controller;

import com.md.accountclient.model.*;
import com.md.accountclient.service.RabbitMQSender;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.math.BigDecimal;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.*;

@Controller
@RequestMapping("/accountclient")
@Slf4j
public class MoneyTransferController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private RabbitMQSender rabbitMQSender;

    @GetMapping("/init")
    public String init(Model model){

        List<Account> accounts = getAccounts();

        MoneyTransferBean moneyTransferBean = new MoneyTransferBean();
        moneyTransferBean.setAccounts(accounts);

        model.addAttribute("moneyTransferBean", moneyTransferBean);

        return "money_transfer";
    }

    private List<Account> getAccounts() {
        List<Account> accounts = new ArrayList<>();
        ResponseEntity<Account[]> response = restTemplate.getForEntity("http://account-service:8081/api/v1/accounts/all/10", Account[].class);
        Account[] accountArray = response.getBody();
        Arrays.stream(accountArray).forEach(accounts::add);
        return accounts;
    }

    @PostMapping("/transfer")
    public String moneyTransfer(MoneyTransferBean moneyTransferBean, Model model){

        MoneyTransferResultBean moneyTransferResultBean = new MoneyTransferResultBean();

        //find actual Account from unitNum and accountNum information
        Long unitNumLong = Long.parseLong(moneyTransferBean.getUnitAndAccount().split("-")[0]);
        BigDecimal unitNum = BigDecimal.valueOf(unitNumLong);
        Long accountNumLong = Long.parseLong(moneyTransferBean.getUnitAndAccount().split("-")[1]);
        BigDecimal accountNum = BigDecimal.valueOf(accountNumLong);

        String readUrl = "http://account-service:8081/api/v1/accounts/account";
        MultiValueMap queryParams = new LinkedMultiValueMap();
        queryParams.add("unitNum", unitNum);
        queryParams.add("accountNum", accountNum);
        URI readUri = UriComponentsBuilder.fromUriString(readUrl).queryParams(queryParams).build().toUri();
        ResponseEntity<Account> response = restTemplate.exchange(readUri, HttpMethod.GET, null ,Account.class);
        Account toAccount = response.getBody();

        //get From Account
        List<Account> accounts = getAccounts();
        Account fromAccount = accounts.stream().filter(a -> a.getId().equals(moneyTransferBean.getFromAccount().getId())).findFirst().orElse(null);

        if(fromAccount != null && toAccount != null){
            // Transfer Operations
            TransferBean transferBean = new TransferBean();
            transferBean.setFromAccount(fromAccount);
            transferBean.setToAccount(toAccount);
            transferBean.setTransactionAmount(moneyTransferBean.getMoney());

            String transferUrl = "http://account-service:8081/api/v1/accounts/transfer";
            HttpEntity<TransferBean> httpEntity = new HttpEntity<>(transferBean);
            ResponseEntity<Boolean> transferResponse = restTemplate.exchange(transferUrl, HttpMethod.POST, httpEntity, Boolean.class);
            Boolean successFlag = transferResponse.getBody();

            moneyTransferResultBean.setSuccessFlag(successFlag);
            if (successFlag){
                moneyTransferResultBean.setMessage(moneyTransferBean.getMoney() + " is transferred successfully to "
                + toAccount.getFirstName() + " " + toAccount.getLastName());
            } else {
                moneyTransferResultBean.setMessage("Money transfer operation is failed!!!");
            }
        }

        if (moneyTransferResultBean.getMessage() != null) {
            log.info("Rabbit MQ Send notification");
            NotificationMessage notificationMessage = new NotificationMessage();
            notificationMessage.setMessage(moneyTransferResultBean.getMessage());
            notificationMessage.setStatusCode("Active");
            notificationMessage.setCreatedTime(LocalDateTime.now().toString());
            rabbitMQSender.send(notificationMessage);
        }

        model.addAttribute("moneyTransferResultBean", moneyTransferResultBean);
        return "money_transfer_result";
    }


}
