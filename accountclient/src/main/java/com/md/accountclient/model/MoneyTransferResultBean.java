package com.md.accountclient.model;

import lombok.Data;

@Data
public class MoneyTransferResultBean {

    private String message;
    private boolean successFlag;

}
