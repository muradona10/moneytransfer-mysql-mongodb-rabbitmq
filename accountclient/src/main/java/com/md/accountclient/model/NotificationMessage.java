package com.md.accountclient.model;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;

@Data
@JsonIdentityInfo(generator = ObjectIdGenerators.IntSequenceGenerator.class, property = "@id", scope = NotificationMessage.class)
public class NotificationMessage {

    private String message;
    private String statusCode;
    private String createdTime;

}
