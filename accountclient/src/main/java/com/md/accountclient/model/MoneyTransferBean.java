package com.md.accountclient.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MoneyTransferBean {

    private List<Account> accounts;
    private Account fromAccount;
    private String unitAndAccount;
    private BigDecimal money;

}
