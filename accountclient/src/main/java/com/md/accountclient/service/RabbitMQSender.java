package com.md.accountclient.service;

import com.md.accountclient.model.NotificationMessage;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class RabbitMQSender {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${accountnotification.rabbitmq.exchange}")
    private String exhange;

    @Value("${accountnotification.rabbitmq.routhingkey}")
    private String routhingkey;

    public void send(NotificationMessage notificationMessage){
        rabbitTemplate.convertAndSend(exhange, routhingkey, notificationMessage);
    }

}
