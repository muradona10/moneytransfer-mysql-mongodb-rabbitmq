# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact


mysql, spring boot app docker commands
Eğer mysql e bağlanırken aşağıdaki gibi bir hata alırsa
Access denied for user 'sa'@'172.17.0.3' (using password: YES)
ssh ile mysql içine girip aşağıdaki komutları yazıp sonra container ı yeniden run etmek gerek
GRANT ALL PRIVILEGES ON *.* TO 'sa'@'172.17.0.2' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON *.* TO 'sa'@'172.17.0.3' IDENTIFIED BY 'password';

--------------------------------------------------------------------------------------------------
SPRING BOOT MYSQL
--------------------------------------------------------------------------------------------------
Spring Boot Mysql İle
docker build -t account-service . [Dockerfile dan image üretme, dockerfile ın olduğu dizinde]
docker run --name account-db -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=accountdb -e MYSQL_USER=sa -e MYSQL_PASWORD=password -d mysql:5.6 [mysql image run etme]
docker container exec -it account-db bash [mysql in içine girip sa ya üstteki gibi yetki verebiliriz, yoksa Access denied for user 'sa'@'172.17.0.3' (using password: YES) hatası alıyor]
docker run -d -p 8080:8080 --name myaccountservice --link account-db account-service [spring boot image i run edip, --link ile account-db ye bağlıyoruz]
docker container logs myaccountservice [ile spring boot uygulamamızın ayağa kalkıp kalkmadığına bakıyoruz]

### To Dockerize ###
* "docker build -t account-notification ." command to create image from Dockerfile on the same folder in Dockerfile
* "docker-compose up command to run images" in docker-compose.yml 
* "docker-compose down" command to stop containers